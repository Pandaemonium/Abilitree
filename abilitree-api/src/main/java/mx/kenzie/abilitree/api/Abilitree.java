package mx.kenzie.abilitree.api;

import com.moderocky.mask.mirror.Mirror;
import mx.kenzie.abilitree.api.data.DataAccessor;
import mx.kenzie.abilitree.api.data.DataAccessorImpl;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.Nullable;

public final class Abilitree {

    public static final Abilitree INSTANCE = new Abilitree();

    private final boolean paper;
    private final String craftVersion;
    private Plugin plugin;
    private DataAccessor accessor;

    private Abilitree() {
        paper = Mirror.classExists("co.aikar.timings.Timing") || Mirror.classExists("com.destroystokyo.paper.util.VersionFetcher");
        craftVersion = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        accessor = new DataAccessorImpl();
    }

    public void register(Plugin plugin, @Nullable DataAccessor accessor) {
        this.plugin = plugin;
        if (accessor != null) this.accessor = accessor;
    }

    public boolean isPaper() {
        return paper;
    }

    public String getCraftVersion() {
        return craftVersion;
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public DataAccessor getAccessor() {
        return accessor;
    }

}
