package mx.kenzie.abilitree.api.data;

import com.google.gson.JsonObject;
import com.moderocky.mask.annotation.Asynchronous;

import java.util.UUID;

public interface DataAccessor {

    default User getUser(UUID uuid) {
        User user = new User(uuid);
        asyncLoad(user);
        return user;
    }

    boolean isLoaded(User user);

    @Asynchronous
    void asyncLoad(User user);

    void load(User user);

    void scheduleSave(User user);

    @Asynchronous
    void asyncSave(User user);

    void save(User user);

}
