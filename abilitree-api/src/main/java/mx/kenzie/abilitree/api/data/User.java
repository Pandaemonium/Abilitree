package mx.kenzie.abilitree.api.data;

import com.google.gson.JsonNull;
import com.google.gson.JsonObject;

import java.util.UUID;

public final class User {

    private final UUID uuid;
    private final JsonObject data;
    public boolean loaded = false;

    public User(UUID uuid) {
        this.uuid = uuid;
        this.data = new JsonObject();
    }

    public User(JsonObject raw) {
        if (!raw.has("uuid") || raw.get("uuid") instanceof JsonNull) throw new IllegalArgumentException("Invalid user data!");
        uuid = UUID.fromString(raw.get("uuid").getAsString());
        data = raw;
    }

    private void apply() {
        if (!data.has(""))
    }

    public void save(boolean async) {

    }

}
